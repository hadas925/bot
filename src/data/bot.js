const mongoose = require('mongoose');
require('./mongoDB');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
 
const Bot = new Schema({
    _id: ObjectId,
    name: String,
    status: String,
    contactDetails: String,
    expertise: [String],
    answer: String
});
const BotModel = mongoose.model('bots', Bot, 'bots_copy');
module.exports = {
    findById:function(id, callback){
        BotModel.findById(id, function (err, bot) {
            if(err){
                console.log(err);
            } else{
                callback(err, bot);
            }
          });
    },
    setFree: function(bot, callback){
        BotModel.updateOne({ _id: bot._id }, { status: 'free' });
    },
    setEngaged: function(bot, callback){
        BotModel.updateOne({ _id: bot._id }, { status: 'engaged' });
    }
};
