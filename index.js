const TelegramBot = require('node-telegram-bot-api');
const config = require('./config.json')
const botData = require('./src/data/bot');

const botDetails = config.default.bot;
const telegramBot = new TelegramBot(botDetails.token, {polling: true});
let bot = null;
let customer = {id:null};

botData.findById(botDetails.id, function(err, result){
  if(err){
    throw 'No bot!';
  }else{
    bot = result;
  }
})

// Reply to /start
telegramBot.onText(/\/start/, (msg, match) => {
  telegramBot.sendMessage(msg.chat.id,'Welcome to your Customer Support Bot!'
  +'commands:\n'
  +'/engage\n'
  +'/askforhelp <topic>\n'
  +'/disengage\n'
  )
});

telegramBot.onText(/\/engage/, (msg, match) => {
  if(!customer.id){
    botData.setEngaged(bot);
    customer.id = msg.chat.id;
    telegramBot.sendMessage(msg.chat.id,'Let\'s start');
  }
});

telegramBot.onText(/\/askforhelp (.+)/, (msg, match) => {
  if(customer.id == msg.chat.id)
  {
    telegramBot.sendMessage(msg.chat.id,'You asked for help. Topic: ' + match[1]
    + '\n My answer: '
    + bot.answer);
  }
});


telegramBot.onText(/\/disengage/, (msg, match) => {
  botData.setFree(bot);
  customer.id = null;
  telegramBot.sendMessage(msg.chat.id,'Good luck! See you!');
});
